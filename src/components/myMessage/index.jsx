import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { deleteMessage, editMessage } from '../../containers/chat/actions';
import './style.css';
import { showPage } from '../../components/editMessage/actions';
class MyMessage extends React.Component {

    editMessage(id) {
        this.props.editMessage(id);
        this.props.showPage();
    }

    render() {
        const { message, created_at, id, likeCount, isEditable } = this.props;
        return (
            <div
                key={id}
                className="message right">
                <div className="message-text">{message}</div>
                <span>{created_at}</span>
                <div>
                    <img
                        className="icon"
                        src='like.svg'
                        alt="Like"
                    />
                    <span>{likeCount}</span>
                    <span
                        className="delete"
                        onClick={() => this.props.deleteMessage(id)}>
                        delete
                    </span>
                    {isEditable ?
                        <span
                            className="edit"
                            onClick={() => this.editMessage(id)}>
                            edit
                        </span> : null}
                </div>
            </div>
        )
    }
}

MyMessage.propTypes = {
    message: PropTypes.string,
    created_at: PropTypes.string,
    id: PropTypes.number,
    deleteMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired,
    showPage: PropTypes.func.isRequired,
    likeCount: PropTypes.number,
    isEditable: PropTypes.bool
}
const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = {
    deleteMessage,
    editMessage,
    showPage
};

export default connect(mapStateToProps, mapDispatchToProps)(MyMessage);
