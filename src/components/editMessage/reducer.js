import { SHOW_PAGE, HIDE_PAGE } from "./actionTypes";

const initialState = {
    canEdit: false
};

export default function (state = initialState, action) {
    switch (action.type) {

        case SHOW_PAGE: {
            return {
                ...state,
                canEdit: true
            };
        }

        case HIDE_PAGE: {
            return {
                ...state,
                canEdit: false
            };
        }

        default:
            return state;
    }
}
