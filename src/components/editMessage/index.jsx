import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import * as actions from './actions';
import './style.css';
import { saveMessage } from '../../containers/chat/actions';
class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editedText: ""
        }
    }

    componentWillReceiveProps(nextProps) {
        const editedText = nextProps.editableMessage.message;
        this.setState({ editedText });
    }

    saveMessage = () => {
        let editedMessage = Object.assign({}, this.props.editableMessage);
        const editedText = this.state.editedText.trim();
        if (editedText === "") {
            return;
        }
        editedMessage.message = editedText;
        this.setState({ editedText: "" });
        this.props.saveMessage(editedMessage);
        this.props.hidePage();
    }

    handleChange = event => {
        const editedText = event.target.value;
        this.setState({ editedText });
    }

    render() {
        if (!this.props.canEdit) {
            return null;
        }
        const editableMessage = this.props.editableMessage;
        return (
            <div className="backdrop" >
                <div className="modal" >

                    <div className="footer">
                        <div>Edit message</div>
                        <input
                            type="text"
                            placeholder='Type something'
                            value={this.state.editedText}
                            onChange={this.handleChange} />
                        <button
                            onClick={this.props.hidePage}>
                            Close
                        </button>
                        <button
                            onClick={this.saveMessage}>
                            Save
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

EditMessage.propTypes = {
    hidePage: PropTypes.func.isRequired,
    editableMessage: PropTypes.object,
    saveMessage: PropTypes.func.isRequired,
    canEdit: PropTypes.bool
};

const mapStateToProps = (state) => {
    return {
        ...state.editMessage,
        editableMessage: state.chat.editableMessage
    }
};

const mapDispatchToProps = {
    ...actions,
    saveMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);