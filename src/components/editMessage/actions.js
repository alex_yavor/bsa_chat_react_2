import { SHOW_PAGE, HIDE_PAGE } from "./actionTypes";

export const showPage = () => ({
    type: SHOW_PAGE
});

export const hidePage = () => ({
    type: HIDE_PAGE
});

