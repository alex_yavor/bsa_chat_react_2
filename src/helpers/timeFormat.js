export default function getFormattedTime(timeString) {
    const time = new Date(timeString.toString());
    let hours = time.getHours();
    let minutes = time.getMinutes();
    if (String(minutes).length === 1) {
        minutes = '0' + minutes
    };
    return new Date(time).toLocaleString('en').slice(0, 10) + ' ' + hours + ':' + minutes;
}
