import React from 'react';
import PropTypes from 'prop-types';

import './style.css'

class Header extends React.Component {


    render() {
        const { chatName,messageCount, lastMessageDate, userCount } = this.props;
        return (
            <header className='header'>
                <div>{chatName}</div>
                <div className="participants">{userCount} participants</div>
                <div className="message-count">{messageCount} messages</div>
                <div className='last-message'>last message at <div>{lastMessageDate}</div></div>
            </header>
        )
    }
}

Header.propTypes = {
    messageCount: PropTypes.number,
    lastMessageDate: PropTypes.string,
    userCount: PropTypes.number,
    chatName:PropTypes.string
}

export default Header;