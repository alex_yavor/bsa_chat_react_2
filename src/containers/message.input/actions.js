import { DROP_MESSAGE_TEXT, HANDLE_CHANGE } from "./actionTypes";


export const dropMessageText = () => ({
    type: DROP_MESSAGE_TEXT,
    payload: {
        messageText: ""
    }
});

export const handleChange = (event) => ({
    type: HANDLE_CHANGE,
    payload: {
        messageText: event.target.value
    }
});

