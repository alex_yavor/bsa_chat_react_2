import { DROP_MESSAGE_TEXT, HANDLE_CHANGE } from "./actionTypes";

const initialState = {
    messageText: ""
}

export default function (state = initialState, action) {
    switch (action.type) {
        case DROP_MESSAGE_TEXT: {
            const { messageText } = action.payload;
            return {
                messageText
            }
        }
        case HANDLE_CHANGE: {
            const { messageText } = action.payload;
            return {
                messageText
            }
        }
       
        default:
            return state;
    }
}
