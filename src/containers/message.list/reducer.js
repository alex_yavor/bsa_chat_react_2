import { INIT_LIST, SCROLL_TO_BOTTOM } from "./actionTypes";

const initialState = {
    messageList: null
}

export default function (state = initialState, action) {
    switch (action.type) {

        case INIT_LIST: {
            const { messageList } = action.payload;
            return {
                messageList
            }
        }

        case SCROLL_TO_BOTTOM: {
            const { messageList } = state;
            const scrollHeight = messageList.scrollHeight;
            const height = messageList.clientHeight;
            const maxScrollTop = scrollHeight - height;
            messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
            return {
                messageList
            }
        }

        default:
            return state;
    }
}
