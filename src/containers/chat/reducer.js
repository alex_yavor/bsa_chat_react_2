import { SAVE_MESSAGE, DELETE_MESSAGE, SEND_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE } from "./actionTypes";
import { getMessages } from '../../services/messageService';
import getFormattedTime from "../../helpers/timeFormat";

const initialState = {
    messages: getMessages(),
    lastId: 1,
    editableMessage: {},
    chatName: "My Chat"
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SAVE_MESSAGE: {
            const { editedMessage } = action.payload;
            const messages = state.messages.map((message) => {
                return message.id === editedMessage.id ? editedMessage : message;
            })
            return {
                ...state,
                messages,
                editableMessage:{},
                canEdit: state.canEdit
            }
        }

        case DELETE_MESSAGE: {
            const { id } = action.payload;
            const filteredMessages = state.messages.filter(message => {
                return message.id !== id;
            })
            return {
                ...state,
                messages: filteredMessages
            };
        }

        case SEND_MESSAGE: {
            const { message } = action.payload;
            const nowDate = getFormattedTime(new Date());
            let messages = state.messages.slice();
            const lastId = state.lastId;
            messages.push({
                created_at: nowDate,
                marked_read: false,
                message,
                user: "Kate",
                isMyMessage: true,
                id: lastId
            })
            return {
                ...state,
                messages,
                lastId: lastId + 1
            }
        }

        case EDIT_MESSAGE: {
            const { id } = action.payload;
            const editableMessage = state.messages.filter((message) => message.id === id)[0];
            return {
                ...state,
                editableMessage
            }
        }

        case LIKE_MESSAGE: {
            const { id } = action.payload;
            let messages = state.messages.slice();
            let message = messages.filter((message) => message.id === id)[0];
            const isLiked = message.isLiked;
            if (!isLiked) {
                isNaN(message.likeCount) ? message.likeCount = 1 : message.likeCount++;
            }
            else { message.likeCount--; }
            message.isLiked = !isLiked;
            return {
                ...state,
                messages
            }
        }

        default:
            return state;
    }
}
