import { SAVE_MESSAGE, DELETE_MESSAGE, SEND_MESSAGE, EDIT_MESSAGE,LIKE_MESSAGE } from "./actionTypes";


export const saveMessage = editedMessage => ({
    type: SAVE_MESSAGE,
    payload: {
        editedMessage
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const sendMessage = message => ({
    type: SEND_MESSAGE,
    payload: {
        message
    }
});

export const editMessage = id => ({
    type: EDIT_MESSAGE,
    payload: {
        id
    }
});

export const likeMessage = id => ({
    type: LIKE_MESSAGE,
    payload: {
        id
    }
});

