import { combineReducers } from "redux";
import chat from "../containers/chat/reducer";
import editMessage from "../components/editMessage/reducer";
import messageInput from "../containers/message.input/reducer";
import messageList from "../containers/message.list/reducer";
const rootReducer = combineReducers({
    chat,
    editMessage,
    messageInput,
    messageList
});

export default rootReducer;
